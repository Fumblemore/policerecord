﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class Issue
    {
        public enum IssueType
        {
            Unknown = -1,
            Offense,
            Crime
        }

        public IssueType Type { get; private set; }
        public string DateOfOccurence { get; private set; }
        public string Description { get; private set; }

        public Issue(IssueType type, string dataOfOccurence, string description)
        {
            Type = type;
            DateOfOccurence = dataOfOccurence;
            Description = description;
        }
    }
}
