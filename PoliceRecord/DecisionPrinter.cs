﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class DecisionPrinter
    {
        public static bool AskYesNoQuestion(string question)
        {
            string answer = "";
            bool isAnswerCorrect = false;

            Console.WriteLine($"{question} [y/n]:");

            while (!isAnswerCorrect)
            {
                answer = Console.ReadLine();
                isAnswerCorrect = answer == "y" || answer == "n";

                if (!isAnswerCorrect)
                    Console.WriteLine("Incorrect answer. Try again");

            }

            return answer == "y";
        }

        public static int AskForPersonFromList(List<Person> people)
        {
            int answer = 1;
            bool isAnswerCorrect = false;

            int i = 1;
            foreach (var e in people)
            {
                ModelPrinter.PrintObjectAsOrderedList(e, i++);
            }

            Console.Write("Please, chose particular person by ordinal: ");

            while (!isAnswerCorrect)
            {
                answer = Int32.Parse(Console.ReadLine());
                isAnswerCorrect = answer > 0 && answer <= people.Count;

                if (!isAnswerCorrect)
                    Console.Write("Incorrect answer. Try again: ");
            }

            return answer - 1;
        }
    }
}
