﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class RecordBook
    {
        private List<Person> _records = new List<Person>();

        public Person GetPersonByPesel(ulong pesel)
        {
            return _records.Find(p => p.Pesel == pesel);
        }

        public bool AddPerson(Person person)
        {
            if (person == null || GetPersonByPesel(person.Pesel) != null)
                return false;

            _records.Add(person);
            return true;
        }

        public List<Person> GetPeopleByName(string name, string surname)
        {
            List<Person> people = new List<Person>();

            _records.ForEach(p => {
                if (p.Name == name && p.Surname == surname)
                    people.Add(p);
            });

            return people;
        }

        public bool RemovePersonByPesel(ulong pesel)
        {
            return _records.RemoveAll(p => p.Pesel == pesel) >= 1;
        }

        public List<Person> GetRecords()
        {
            return _records;
        }
    }
}
