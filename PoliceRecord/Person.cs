﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class Person
    {
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string BirthDate { get; private set; }
        public string City { get; private set; }

        public ulong Pesel { get; private set; }

        public List<Issue> Issues { get; private set; } = new List<Issue>();

        public Person(
            string name, 
            string surname, 
            string birthDate, 
            string city, 
            ulong pesel)
        {
            Name = name;
            Surname = surname;
            BirthDate = birthDate;
            City = city;
            Pesel = pesel;
        }

        public void AddIssue(Issue issue)
        {
            Issues.Add(issue);
        }
    }
}
