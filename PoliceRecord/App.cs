﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class App
    {
        private CommandDispatcher _commandDispatcher;
        private RecordBook _recordBook;
        private bool _exit;

        public App()
        {
            _recordBook = new RecordBook();
            _commandDispatcher = new CommandDispatcher();
            _exit = false;
        }

        public void Run()
        {
            InitializeCommands();
            RunExecutionLoop();
        }

        private void InitializeCommands()
        {
            _commandDispatcher.AddCommand("Add", "Adds a new person to record book", AddRecordAction);
            _commandDispatcher.AddCommand("PrintAll", "Prints all records from record book", PrintAllRecordsAction);
            _commandDispatcher.AddCommand("PrintByPesel", "Prints data and felonies of person with given PESEL", PrintByPeselAction);
            _commandDispatcher.AddCommand("PrintByName", "Prints data and felonies of person with given name", PrintByNameAction);
            _commandDispatcher.AddCommand("RemoveByPesel", "Remove a person with given PESEL from record", RemoveByPeselAction);
            _commandDispatcher.AddCommand("RemoveByName", "Remove a person with given name from record", RemoveByNameAction);
            _commandDispatcher.AddCommand("AddIssueByPesel", "Adds issue to a peson with given PESEL", AddIssueByPeselAction);
            _commandDispatcher.AddCommand("AddIssueByName", "Adds issue to a peson with given name", AddIssueByNameAction);
            _commandDispatcher.AddCommand("Exit", "Closes the aplication", ExitAction);
            _commandDispatcher.AddCommand("Help", "Prints all valid commands", HelpAction);
        }

        private void RunExecutionLoop()
        {
            string command;

            Console.WriteLine("To see available commands, type 'Help' and press Enter");

            while (!_exit)
            {
                Console.Write("\nPlease, type the command: ");
                command = Console.ReadLine();

                if (!_commandDispatcher.Dispatch(command))
                    if(_commandDispatcher.ConstainsCommand(command))
                        Console.WriteLine($"Some unhadled error occured when executing {command} command action");
                    else
                        Console.WriteLine($"Unknow command: {command}");
            }
        }

        private bool AddRecordAction()
        {
            string name;
            string surname;
            string birthDate;
            string city;
            ulong pesel;

            Console.Write("Name:       ");
            name = Console.ReadLine();

            Console.Write("Surname:    ");
            surname = Console.ReadLine();

            Console.Write("Birth date: ");
            birthDate = Console.ReadLine();

            Console.Write("City:       ");
            city = Console.ReadLine();

            Console.Write("PESEL:      ");
            pesel = UInt64.Parse(Console.ReadLine());

            Person newPerson = new Person(name, surname, birthDate, city, pesel);

            if (_recordBook.AddPerson(newPerson))
            {
                Console.WriteLine($"Person successfuly added (PESEL: {newPerson.Pesel})");
            }
            else
            {
                Person person = _recordBook.GetPersonByPesel(pesel);

                if (person == null)
                    Console.WriteLine($"Some error occured when adding person with {pesel} PESEL");
                else
                {
                    Console.WriteLine("Person with same PESEL already exists in Record Book.");

                    if (DecisionPrinter.AskYesNoQuestion("Do you want to see person details?"))
                        ModelPrinter.PrintObjectData(person);
                }
            }

            return true;
        }

        private bool PrintAllRecordsAction()
        {
            List<Person> records = _recordBook.GetRecords();

            if (records.Count == 0)
                Console.WriteLine("No records in the Record book");
            else
            {
                foreach (var e in records)
                {
                    ModelPrinter.PrintObjectData(e);
                }
            }

            return true;
        }

        private bool PrintByPeselAction()
        {
            ulong pesel;

            Console.Write("PESEL: ");
            pesel = UInt64.Parse(Console.ReadLine());

            Person person = _recordBook.GetPersonByPesel(pesel);

            if (person == null)
                Console.WriteLine($"There is nobody with PESEL {pesel} recorded");
            else
            {
                ModelPrinter.PrintObjectData(person);
                List<Issue> issues = person.Issues;

                foreach (var e in issues)
                {
                    ModelPrinter.PrintObjectData(e, "\t");
                }
            }

            return true;
        }

        private bool PrintByNameAction()
        {
            string name;
            string surname;

            Console.WriteLine("Name:    ");
            name = Console.ReadLine();

            Console.WriteLine("Surname: ");
            surname = Console.ReadLine();

            List<Person> people = _recordBook.GetPeopleByName(name, surname);

            if (people.Count == 0)
                Console.WriteLine($"There is nobody with name {name} {surname} recorded");
            else if (people.Count() == 1)
            {
                var person = people.First();

                ModelPrinter.PrintObjectData(person);

                List<Issue> issues = person.Issues;
                foreach (var e in issues)
                {
                    ModelPrinter.PrintObjectData(e, "\n");
                }
            }
            else
            {
                Console.WriteLine($"Multiple eople with name {name} {surname} found: ");
                int index = DecisionPrinter.AskForPersonFromList(people);

                ModelPrinter.PrintObjectData(people[index]);
                List<Issue> issues = people[index].Issues;

                foreach (var e in issues)
                {
                    ModelPrinter.PrintObjectData(e, "\t");
                }
            }

            return true;
        }

        private bool RemoveByPeselAction()
        {
            ulong pesel;
            Console.Write("PESEL: ");
            pesel = UInt64.Parse(Console.ReadLine());

            Person person = _recordBook.GetPersonByPesel(pesel);

            if (person == null)
                Console.WriteLine($"There is nobody with PESEL {pesel} recorded");
            else
            {
                if(_recordBook.RemovePersonByPesel(pesel))
                    Console.WriteLine($"Person successfuly removed (PESEL: {pesel})");
                else
                    Console.WriteLine($"Some error occured when removing person with {pesel} PESEL");
            }

            return true;
        }

        private bool RemoveByNameAction()
        {
            string name;
            string surname;

            Console.Write("Name:    ");
            name = Console.ReadLine();

            Console.Write("Surname: ");
            surname = Console.ReadLine();

            var people = _recordBook.GetPeopleByName(name, surname);

            if (people.Count == 0)
            {
                Console.WriteLine($"There is nobody with name {name} {surname} recorded");
                return true;
            }

            int index;

            if (people.Count == 1)
                index = 0;
            else
            {
                Console.WriteLine($"Multiple ppeople with name {name} surname found: ");
                index = DecisionPrinter.AskForPersonFromList(people);
            }

            ulong pesel = people[index].Pesel;

            if (_recordBook.RemovePersonByPesel(pesel))
                Console.WriteLine($"Person successfuly removed (PESEL: {pesel})");
            else
                Console.WriteLine($"Some error occured when removing person {pesel} PESEL");

            return true;
        }

        public bool AddIssueByPeselAction()
        {
            ulong pesel;
            Console.Write("PESEL: ");
            pesel = UInt64.Parse(Console.ReadLine());

            Person person = _recordBook.GetPersonByPesel(pesel);

            if (person == null)
                Console.WriteLine($"There is nobody with PESEL {pesel} recorded");
            else
            {
                AddIssue(person);
            }

            return true;
        }

        public bool AddIssueByNameAction()
        {
            string name;
            string surname;

            Console.Write("Name:    ");
            name = Console.ReadLine();

            Console.Write("Surname: ");
            surname = Console.ReadLine();

            var people = _recordBook.GetPeopleByName(name, surname);

            if (people.Count == 0)
                Console.WriteLine($"There is nobody with name {name} {surname} recorded");
            else if (people.Count == 1)
            {
                AddIssue(people.First());
            }
            else
            {
                Console.WriteLine($"Multiple people with name {name} {surname} found: ");
                int index = DecisionPrinter.AskForPersonFromList(people);

                AddIssue(people[index]);
            }

            return true;
        }

        private void AddIssue(Person person)
        {
            Issue.IssueType type = Issue.IssueType.Unknown;
            string dateOfOccurence;
            string description;

            Console.Write("Issue type:      ");

            bool isIssueTypeCorrect = false;
            while (!isIssueTypeCorrect)
            {
                type = IssueTypeConverter.ConvertStringToIssueType(Console.ReadLine());

                isIssueTypeCorrect = type != Issue.IssueType.Unknown;

                if (!isIssueTypeCorrect)
                    Console.WriteLine("Incorrect issue type. Try again: ");
            }

            Console.Write("Date of Occurence: ");
            dateOfOccurence = Console.ReadLine();

            Console.Write("Description: ");
            description = Console.ReadLine();

            Issue issue = new Issue(type, dateOfOccurence, description);
            person.AddIssue(issue);

            Console.WriteLine($"Issue successfuly added (PESEL: {person.Pesel})");
        }

        public bool ExitAction()
        {
            _exit = true;
            return true;
        }

        public bool HelpAction()
        {
            foreach (var e in _commandDispatcher.GetValidCommandActions())
            {
                ModelPrinter.PrintObjectData(e);
            }

            return true;
        }

    }
}
