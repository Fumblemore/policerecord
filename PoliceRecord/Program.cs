﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to GDPD Records System");

            App app = new App();
            app.Run();

            Console.WriteLine("Closing the GDPD Records System");
            return;
        }
    }
}
