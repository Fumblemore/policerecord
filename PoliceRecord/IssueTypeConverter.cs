﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class IssueTypeConverter
    {
        public static Issue.IssueType ConvertStringToIssueType(string issueTypeString)
        {
            try
            {
                return (Issue.IssueType) Enum.Parse(typeof(Issue.IssueType), issueTypeString);
            }
            catch
            {
                return Issue.IssueType.Unknown;
            }
        }

        public static string ConvertIssueTypeToString(Issue.IssueType issueType)
        {
            return issueType.ToString();
        }
    }
}
