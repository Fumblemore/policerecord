﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceRecord
{
    class ModelPrinter
    {
        public static void PrintObjectData(Person person)
        {
            Console.WriteLine();
            Console.WriteLine($"Name:       {person.Name}");
            Console.WriteLine($"Surname:    {person.Surname}");
            Console.WriteLine($"Birth date: {person.BirthDate}");
            Console.WriteLine($"City:       {person.City}");
            Console.WriteLine($"PESEL:      {person.Pesel}");
        }

        public static void PrintObjectData(Issue issue, string prefix)
        {
            Console.WriteLine();
            Console.WriteLine($"Type:              {IssueTypeConverter.ConvertIssueTypeToString(issue.Type)}");
            Console.WriteLine($"Date of Occurence: {issue.DateOfOccurence}");
            Console.WriteLine($"Description:       {issue.Description}");
        }

        public static void PrintObjectData(CommandAction command)
        {
            Console.WriteLine($"{command.Command} - {command.Description}");
        }

        public static void PrintObjectAsOrderedList(Person person, int ordinal)
        {
            Console.WriteLine($"{ordinal}. {person.Name} {person.Surname} (PESEL: {person.Pesel})");
        }
    }
}
